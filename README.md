### SPA using
- React
- React router

### Requirements
- Nodejs v6

### Installation
`yarn install;`

### Development server
`yarn run dev`

### Production build
`yarn run build`