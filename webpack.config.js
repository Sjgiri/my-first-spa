'use strict';

const webpack                     = require('webpack'),
    CleanWebpackPlugin            = require('clean-webpack-plugin'),
    HtmlWebpackPlugin             = require('html-webpack-plugin'),
    OfflinePlugin                 = require('offline-plugin'),
    LodashModuleReplacementPlugin = require('lodash-webpack-plugin'),
    BundleAnalyzerPlugin          = require('webpack-bundle-analyzer').BundleAnalyzerPlugin,
    path                          = require('path');

module.exports = {
    cache: true,
    context: path.join(__dirname, "src"),
    entry: {
        app: path.resolve(__dirname, 'src/js/app'),
        vendor: [
            'react',
            'react-dom',
            'react-router',
            'lodash/map',
            'lodash/split',
            'lodash/filter',
            'lodash/last',
            'react-bootstrap-table',
            'whatwg-fetch',
        ]
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    presets: [
                        'react',
                        ['es2015', { modules: false }],
                    ],
                    plugins: [
                        'react-html-attrs',
                        'transform-class-properties',
                        'lodash',
                    ],
                    cacheDirectory: true
                }
            },
            {
                test: /\.hbs$/,
                loader: 'handlebars-loader'
            },
        ]
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "[name].[hash].js",
        chunkFilename: '[name].[hash].js',
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {NODE_ENV: JSON.stringify('production')}
        }),
        new CleanWebpackPlugin(['dist'], {
            verbose: true, // log to console
            dry    : false // true for testing
        }),
        new LodashModuleReplacementPlugin({
            'collections': true,
            'shorthands': true
        }),
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            filename:'[name].[hash].js',
            minChunks: Infinity
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                screw_ie8: true,
                conditionals: true,
                unused: true,
                comparisons: true,
                sequences: true,
                dead_code: true,
                evaluate: true,
                join_vars: true,
                if_return: true
            },
            output: {
                comments: false
            }
        }),
        new HtmlWebpackPlugin({
            hash: false,
            filename: 'index.html',
            template: __dirname + '/src/index.html',
        }),
        new OfflinePlugin(),
        new BundleAnalyzerPlugin(),
    ],
};