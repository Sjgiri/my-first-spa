import React from "react";

export default class Footer extends React.Component {
  render() {
    return (
      <footer class="bs-docs-footer">
        <div class="container">
          <div class="col-lg-12">
            <p>Footer. All rights reserved</p>
          </div>
        </div>
      </footer>
    );
  }
}
