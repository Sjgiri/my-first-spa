import React, {Component} from 'react';
import 'whatwg-fetch';

export default class Planet extends Component {
    constructor(props) {
        super(props);

        this.state = {
            planet: {}
        };

        this.url = 'https://swapi.co/api/planets';
    }

    componentDidMount() {
        const { id } = this.props.params;

        this.getPlanet(id)
            .then((resp) => {
                this.setState({ planet: resp });
            })
            .catch((ex) => {
                console.log('setting state failed', ex)
            });
    }

    getPlanet(id) {
        return fetch(`${this.url}/${id}`)
            .then((response) => {
                return response.json();
            })
            .catch((ex) => {
                console.log('parsing failed', ex)
            });
    }

    render() {
        const { name, terrain, climate, gravity, population, url } = this.state.planet;

        return (
            <div class="jumbotron">
                <h1>{ name }</h1>
                <p>Terrain: { terrain }</p>
                <p>Climate: { climate }</p>
                <p>Gravity: { gravity }</p>
                <p>Population: { population }</p>
                <p>Url: <a href={ url }> { url }</a></p>
            </div>
        );
    }
}
