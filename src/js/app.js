import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';

import Archives from './pages/Archives';
import Planets from './pages/Planets';
import Planet from './components/Planet';
import Layout from './pages/Layout';
import Settings from './pages/Settings';
import PageNotFound from './components/PageNotFound';

const app = document.getElementById('app');

// register service worker
require('offline-plugin/runtime').install();

const routes = (
  <Router history={hashHistory}>
    <Route path="/" component={Layout}>
      <IndexRoute component={Planets}></IndexRoute>
      <Route path="planets/:id" component={Planet}></Route>
      <Route path="archives" component={Archives}></Route>
      <Route path="settings" component={Settings}></Route>
      <Route path="*" component={PageNotFound} />
    </Route>
  </Router>
);

ReactDOM.render(routes, app );
