import React, {Component} from 'react';
import Planet from '../components/Planet';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import map from 'lodash/map';
import split from 'lodash/split';
import filter from 'lodash/filter';
import last from 'lodash/last';
import 'whatwg-fetch';
import { Link } from 'react-router';

export default class Planets extends Component {
    constructor(props) {
        super(props);

        this.state = {
            planets: []
        };

        this.url = 'https://swapi.co/api/planets/';

        this.renderAnchor = this.renderAnchor.bind(this);
    }

    componentDidMount() {
        this.getPlanets()
            .then((resp) => {
                this.setState({ planets: resp.results });
            })
            .catch((ex) => {
                console.log('setting state failed', ex)
            });
    }

    getPlanets() {
        return fetch(this.url)
            .then((response) => {
                return response.json();
            })
            .catch((ex) => {
                console.log('parsing failed', ex)
            });
    }

    renderAnchor (cell, row) {
        const id = this.getIdFromUrl(row);

        return <Link to={ `planets/${id}` }> {id} </Link>;
    }

    getIdFromUrl(row) {
        return last(filter(split(row.url, '/')));
    }

    render() {
        const options = {
            page: 1,  // which page you want to show as default
            sizePerPageList: [
                {
                    text: '5',
                    value: 5
                },
                {
                    text: '7',
                    value: 7
                },
                {
                    text: 'All',
                    value: this.state.planets.length
                }
            ],
            sizePerPage: 5,  // which size per page you want to locate as default
            pageStartIndex: 0, // where to start counting the pages
            paginationSize: 3,  // the pagination bar size.
            prePage: 'Prev', // Previous page button text
            nextPage: 'Next', // Next page button text
            firstPage: 'First', // First page button text
            lastPage: 'Last', // Last page button text
            paginationShowsTotal: false,  // Accept bool or function
            hideSizePerPage: true //You can hide the dropdown for sizePerPage
        };

        return (
            <BootstrapTable data={ this.state.planets } options={ options } striped hover pagination search exportCSV>
                <TableHeaderColumn isKey dataField='url'  dataFormat={ this.renderAnchor }>Id</TableHeaderColumn>
                <TableHeaderColumn dataField='name'>Name</TableHeaderColumn>
                <TableHeaderColumn dataField='diameter'>Diameter</TableHeaderColumn>
                <TableHeaderColumn dataField='climate'>Climate</TableHeaderColumn>
            </BootstrapTable>
        );
    }
}
