'use strict';

const webpack           = require('webpack'),
    CleanWebpackPlugin  = require('clean-webpack-plugin'),
    HtmlWebpackPlugin   = require('html-webpack-plugin'),
    OfflinePlugin       = require('offline-plugin'),
    path                = require('path');

module.exports = {
    cache: true,
    context: path.join(__dirname, "src"),
    entry: {
        app: path.resolve(__dirname, 'src/js/app'),
        vendor: [
            'react',
            'react-dom',
            'react-router',
            'lodash/map',
            'lodash/split',
            'lodash/filter',
            'lodash/last',
            'react-bootstrap-table',
            'whatwg-fetch',
        ]
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    presets: [
                        'react',
                        ['es2015', { modules: false }],
                    ],
                    plugins: [
                        'react-html-attrs',
                        'transform-class-properties'
                    ],
                    cacheDirectory: true
                }
            },
            {
                test: /\.hbs$/,
                loader: 'handlebars-loader'
            },
        ]
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "[name].js",
        chunkFilename: '[name].js',
    },
    plugins: [
        new HtmlWebpackPlugin({
            hash: true,
            filename: 'index.html',
            template: __dirname + '/src/index.html',
        }),
    ],
    devServer: {
        stats: {
            colors: true,
            hash: false,
            version: false,
            timings: false,
            assets: false,
            chunks: false,
            modules: false,
            reasons: false,
            children: false,
            source: false,
            errors: true,
            errorDetails: true,
            warnings: true,
            publicPath: true
        },
    },
};